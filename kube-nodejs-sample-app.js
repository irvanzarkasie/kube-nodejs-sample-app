const express = require("express");
const dotenv = require("dotenv");

// Enable express app
const app = express();

// Enable express to parse json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialize environment variables
dotenv.config();

app.get("/",(req, res) => {
  res.send("Hello world from " + process.env.HOSTNAME);
});

var server = app.listen(process.env.APP_PORT, function () {
  console.log(new Date().toISOString() + " " + process.env.APP_NAME + " running on server", server.address());
  console.log(new Date().toISOString() + " " + process.env.APP_NAME + " running on port", server.address().port);
});
