FROM node:8-alpine

RUN npm install express

RUN npm install dotenv

COPY .env .

COPY kube-nodejs-sample-app.js .

CMD node kube-nodejs-sample-app.js

EXPOSE 3000
